﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class LoadSystemLanguageOnStart : MonoBehaviour {

	public static string savedLanguage = "en";

	// Use this for initialization
	void Start () {
		LanguageManager lm = LanguageManager.Instance;

			lm.ChangeLanguage(savedLanguage);


	}
}
