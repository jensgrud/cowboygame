﻿using UnityEngine;
using System.Collections;

public class BulletHole : MonoBehaviour {

    public float bulletDecayTime = 3f;
    float creationTime;

	// Use this for initialization
	void Start () {
        this.creationTime = Timer.Instance.currentValue;
	}
	
	// Update is called once per frame
	void Update () {
        if (this.creationTime + this.bulletDecayTime <= Timer.Instance.currentValue)
            GameObject.Destroy(this.gameObject);
	}
}
