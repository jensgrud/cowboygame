﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;

public class GUIManager : MonoBehaviour {
    public static GUIManager Instance
    {
        get;
        private set;
    }

	//For rotating the lavel
	private float rotAngle = 0;

	//For fading the points on tap
	private Color myColor;
	private float alpha = 1f;
	public float explosionFadeSpeed = 0.5f;
    public float scoreFadeSpeed = 0.5f;
	private float targetAlpha = 0;
	
	//for showing the points an object gives when tapped
	private bool showTap = true;
	public float timeBetweenPoints = 1.0f;
	private Vector3 canHitPosition;
	private Vector2 pointTapSize = new Vector2 (50f, 20f);

	// For displaying bonus item text
	private string bonusText = "";
	public Vector2 bonusTextRectSize = new Vector2 (200f, 30f);
	public GUIStyle bonusTextStyle = new GUIStyle ();

	//For sprite on hit effect
	public GameObject canHitEffect;
	public Vector2 canHitEffectSize = new Vector2(150f, 150f);
	private List<GUITexture> hitEffects = new List<GUITexture>();

	//for scaling GUI
	private float standardRatio;
	private float screenRatio;
	private float conversionRate;
	private float standardWidth;
	private float standardHeight;
	private float widthRate;
	private float heightRate;
    #region public fields
    public GUISkin GameSkin;

    #region timer fields
    public GUIStyle timerStyle = new GUIStyle();
    #endregion
    
	#region score fields
    public GUIStyle scoreStyle = new GUIStyle();
	#endregion
    
    #region ammo fields
	public Texture[] bulletTextures;
    #endregion

    #region bullethole fields
    public GameObject bulletHole;
    public Vector2 bulletHoleSize = new Vector2(16f, 16f);
    #endregion

    #region endGame fields
    public bool endGame = false;
	public Vector2 endScoreSize = new Vector2 (200, 50);
	public Vector2 endSentenceSize = new Vector2 (200, 50);
	public Vector2 restartButtonSize = new Vector2 (400, 100);
	#endregion

    #region combo fields
    public float comboTextDecayTime = 3.0f;
    #endregion

    #region hitEffect fields
    public GUIStyle onHitEffectStyle = new GUIStyle();
    #endregion

    #endregion

    bool canHitEventRegistered = false;
    float comboTextEnabledTime = .0f;

    GameObject AmmoTexture;
    GameObject CanTexture;
    GameObject DrunkenHazeTexture;
    GUIText ComboText;

    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start () {
        AmmoTexture = transform.FindChild("AmmoTexture").gameObject;
		CanTexture = transform.FindChild("CanTexture").gameObject;
        DrunkenHazeTexture = transform.FindChild("DrunkenHazeTexture").gameObject;
        DrunkenHazeTexture.GetComponent<GUITexture>().enabled = false;
        DrunkenHazeTexture.GetComponent<GUITexture>().pixelInset = new Rect(0, 0, Screen.width, Screen.height);

        ComboText = transform.FindChild("ComboText").gameObject.GetComponent<GUIText>();
        ComboText.enabled = false;

        ScoreManager.Instance.OnComboStart += ScoreManager_OnComboStart;
        ScoreManager.Instance.OnComboEnd += ScoreManager_OnComboEnd;
		standardRatio =(float) 16 / 9; //the gui was designed for 16:9 (1618x910 pixels)
		standardWidth = 1618f;
		standardHeight = 910f; 
		screenRatio = (float) Screen.width / Screen.height;
		conversionRate = screenRatio / standardRatio;
		widthRate = Screen.width / standardWidth;
		heightRate = Screen.height / standardHeight;
	}

    void ScoreManager_OnComboEnd(object sender, ComboEventArgs e)
    {
        ComboText.text = string.Format(LanguageManager.Instance.GetTextValue("gui.comboEnd"), e.comboScore);
        ComboText.enabled = true;
        comboTextEnabledTime = Timer.Instance.currentValue;
    }

    void ScoreManager_OnComboStart(object sender)
    {
        ComboText.text = LanguageManager.Instance.GetTextValue("gui.comboStart");
        ComboText.enabled = true;
        comboTextEnabledTime = Timer.Instance.currentValue;
    }

    void GUIManager_OnCanHit(object sender)
    {
        //GenerateBulletHole();
		EffectOnTap ();
		//CanAnimation.Instance.CanAnimate();//CanXpos, CanYpos);
		
    }

    void OnGUI()
    {
        GUI.skin = this.GameSkin;

        
 //       this.Timer_OnGUI();
        this.Score_OnGUI();
//        this.Ammo_OnGUI();
//        this.Can_OnGUI();
        this.DrunkenHaze_OnGUI();

		if (endGame)
			this.GameOver_OnGui ();

		//Fading the alpha towards 0
		if( !Mathf.Approximately( alpha, targetAlpha ) )
		{
			alpha = Mathf.MoveTowards( alpha, targetAlpha, scoreFadeSpeed * Time.deltaTime);
		}
		PointsOnTapOnGUI();
	}

    void DrunkenHaze_OnGUI()
    {
        GUITexture gTex = DrunkenHazeTexture.GetComponent<GUITexture>();
        if (GameManager.Instance.IsSlowedDown())
            gTex.enabled = true;
        else
            gTex.enabled = false;
    }
	
	void Timer_OnGUI()
    {

		timerStyle.fontSize = (int) (72*conversionRate*widthRate);
		Vector2 contentOffset = timerStyle.contentOffset;
		contentOffset.x = Screen.width * 0.05f;
		contentOffset.y = 0f;
		timerStyle.contentOffset = contentOffset;
		GUI.Label(new Rect(), 
		    LanguageManager.Instance.GetTextValue("gui.time")  + (int)Timer.Instance.currentValue + " s", timerStyle);
    }
    
	void Score_OnGUI()
	{
		scoreStyle.fontSize = (int) (36*conversionRate*widthRate);
		Vector2 contentOffset = scoreStyle.contentOffset;
		contentOffset.x = Screen.width * 0.03f;
		contentOffset.y = Screen.height * 0.03f;
		scoreStyle.contentOffset = contentOffset;
	//	scoreStyle.contentOffset.y = timerStyle.fontSize;
		GUI.Label(new Rect(), 
		          LanguageManager.Instance.GetTextValue("gui.score") + (int)ScoreManager.Instance.currentScore + " pt", scoreStyle);
	}


//    void Ammo_OnGUI()
//    {
//		Rect pixelInset = AmmoTexture.GetComponent<GUITexture> ().pixelInset;
//		pixelInset.x =10 * conversionRate * widthRate;
//		pixelInset.y = 10 * conversionRate * heightRate;
//		pixelInset.width = 128 * conversionRate * widthRate;
//		pixelInset.height = 128 * conversionRate * heightRate;
//        AmmoTexture.GetComponent<GUITexture>().texture = bulletTextures[ShootingManager.Instance.currentAmountOfBullets];
//		AmmoTexture.GetComponent<GUITexture> ().pixelInset = pixelInset;
//
//    }

//    void Can_OnGUI()
//    { 	
//		float CanXpos = Screen.width - (CanTexture.GetComponent<GUITexture> ().pixelInset.width);
//		float CanYpos = 50f;
//		GUI.Label(new Rect(
//				CanXpos,
//				CanYpos,
//				CanTexture.GetComponent<GUITexture>().pixelInset.width,
//				CanTexture.GetComponent<GUITexture>().pixelInset.height
//			),
//		          LanguageManager.Instance.GetTextValue("gui.can_hits") + Can.Instance.hitAmount
//		);
//
//        if (!canHitEventRegistered)
//        {
//            foreach (Shootable jug in GameManager.Instance.ObjectsInTheAir)
//                if (jug is Can)
//                    ((Can)jug).OnCanHit += GUIManager_OnCanHit;
//
//            canHitEventRegistered = true;
//        }
//    }

	void Update () {
        //ComboText delay
        if (ComboText.enabled && comboTextEnabledTime + comboTextDecayTime < Timer.Instance.currentValue)
            ComboText.enabled = false;        

		List<GUITexture> hitEffectsToDestroy = new List<GUITexture>();

        //What is this?
		foreach (GUITexture effect in hitEffects) {
			myColor = effect.color;


			if( !Mathf.Approximately( effect.color.a, targetAlpha ) )
			{
                myColor.a = Mathf.MoveTowards(effect.color.a, targetAlpha, explosionFadeSpeed * Time.deltaTime);
			}	
			effect.color = myColor;
			if (effect.color.a < 0.1f){
				GameObject.Destroy(effect.transform.parent.gameObject);
				hitEffectsToDestroy.Add(effect);
			}
		}
		
		foreach (GUITexture effect in hitEffectsToDestroy) {
			hitEffects.Remove(effect);
		}

//		standardRatio =(float) 16 / 9; //the gui was designed for 16:9 (1618x910 pixels)
//		standardWidth = 1618f;
//		standardHeight = 910f; 
//		screenRatio = (float) Screen.width / Screen.height;
//		conversionRate = screenRatio / standardRatio;
//		widthRate = Screen.width / standardWidth;
//		heightRate = Screen.height / standardHeight;
	}


	void EffectOnTap(){
		myColor = GUI.color;
		myColor.r = 255;
		//GUI.color = myColor;
		//Create background sprite
		
		GameObject hitEffect = (GameObject)Instantiate(this.canHitEffect);
		GUITexture hitEffectTexture = hitEffect.GetComponentInChildren<GUITexture>();
		hitEffects.Add (hitEffectTexture);

//
		hitEffectTexture.pixelInset = new Rect(canHitPosition.x - (hitEffectTexture.pixelInset.width/2)*conversionRate*widthRate,
		                                       canHitPosition.y - (hitEffectTexture.pixelInset.height/2)*conversionRate*heightRate,
		                                       hitEffectTexture.pixelInset.width*conversionRate*widthRate,
		                                       hitEffectTexture.pixelInset.height*conversionRate*heightRate);

	
	}

	void PointsOnTapOnGUI(){
		myColor = GUI.color;
		myColor.a = alpha;
		GUI.color = myColor;
		onHitEffectStyle.fontSize = (int) (72*conversionRate*widthRate);
		GUI.Label (new Rect (canHitPosition.x - pointTapSize.x, Screen.height - canHitPosition.y - pointTapSize.y/2, onHitEffectStyle.contentOffset.x*conversionRate*widthRate, onHitEffectStyle.contentOffset.y*conversionRate*heightRate),"+" + (int)ScoreManager.Instance.points, onHitEffectStyle);
		
        BonusItemText ();
		//Create background sprite
	}

	void BonusItemText(){
		//GUI.contentColor = Color.green;
		bonusTextStyle.fontSize = (int) (72*conversionRate*widthRate);
		Vector2 contentOffset = bonusTextStyle.contentOffset;
		contentOffset.x = 100*conversionRate * widthRate;
		contentOffset.y = -100*conversionRate * heightRate;
		bonusTextStyle.contentOffset = contentOffset;
		float xPosition = canHitPosition.x - pointTapSize.x;
		float yPosition=  Screen.height - pointTapSize.y/2 -canHitPosition.y + 20*conversionRate*heightRate;
		float xScale = 0;
		float yScale = 0;
		GUI.Label (new Rect (xPosition, yPosition, xScale, yScale),bonusText, bonusTextStyle);
	}

    void GameOver_OnGui(){

		Application.LoadLevel ("EndingMenu");
    }

    public void GenerateBulletHole()
    {
        GUITexture canTex = CanTexture.GetComponent<GUITexture>();

        //Take half to compensate for negative coords
        float sWidth = Screen.width / 2;
        float sHeight = Screen.height / 2;

        //Create bullet hole
        GameObject bHole = (GameObject)Instantiate(this.bulletHole);
        GUITexture bHoleTexture = bHole.GetComponentInChildren<GUITexture>();
        bHoleTexture.pixelInset = new Rect(
            Random.Range(
                sWidth + canTex.pixelInset.x,
                sWidth + canTex.pixelInset.x + canTex.pixelInset.width - bulletHoleSize.x),
            Random.Range(
                sHeight + canTex.pixelInset.y,
                sHeight + canTex.pixelInset.y + canTex.pixelInset.height - bulletHoleSize.y),
            bulletHoleSize.x,
            bulletHoleSize.y);
    }

	public void ShowPointsOnTap(Vector3 position){
		alpha = 1f; //Resets the alpha of the point label
		canHitPosition = position;
		bonusText = "";
		EffectOnTap ();
	}

	public void ShowPointsOnTap(Vector3 position, Bonus item){
		alpha = 1f; //Resets the alpha of the point label
		canHitPosition = position;
		if (item != null) {
			bonusText = item.bonusText;
		} else {
			bonusText = "";
		}
		EffectOnTap ();
	}
	
}
