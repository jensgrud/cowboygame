﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {

	public GameObject SmokePrefab;
	private float standardRatio;
	private float screenRatio;
	private float conversionRate;
	private float standardWidth;
	private float standardHeight;
	private float widthRate;
	private float heightRate;

	void Start(){
		standardRatio =(float) 16 / 9; //the gui was designed for 16:9 (1618x910 pixels)
		standardWidth = 1618f;
		standardHeight = 910f; 
		screenRatio = (float) Screen.width / Screen.height;
		conversionRate = screenRatio / standardRatio;
		widthRate = Screen.width / standardWidth;
		heightRate = Screen.height / standardHeight;

	}

	void OnCollisionEnter(Collision c){
		
		if (c.gameObject.GetComponent<Shootable>() != null && !c.gameObject.GetComponent<Shootable>().isDead){
		
			c.gameObject.GetComponent<Shootable>().isDead = true;
			
			if (c.gameObject.GetComponent<Can>() != null) {
			
				if (audio != null)
					audio.Play();
					
				GameObject smoke = Instantiate(SmokePrefab) as GameObject;
				smoke.transform.parent = c.transform.parent;
				smoke.transform.position = c.transform.position;
				smoke.transform.rotation = c.transform.rotation;
				smoke.transform.parent = transform;	
				GameObject.Destroy(smoke.gameObject,2f);
				GameObject.Find("GameOverText").guiText.fontSize =(int) (72*conversionRate*widthRate);
				iTween.MoveTo(GameObject.Find("GameOverText"),new Vector3(0.5f,0f,0f),1f);
			
				GameManager.Instance.Invoke("EndGame",2f);
				
			} else {
			
				c.gameObject.AddComponent<BlendOut>();
				
				if (SmokePrefab != null){
				
					GameObject smoke = Instantiate(SmokePrefab) as GameObject;
					smoke.transform.parent = c.transform.parent;
					smoke.transform.position = c.transform.position;
					smoke.transform.rotation = c.transform.rotation;
					smoke.transform.parent = transform;
					GameObject.Destroy(smoke.gameObject,1f);
				}
				
				
				GameObject.Destroy(c.gameObject,1f);
			}
			
		}
				
		
	}
}
