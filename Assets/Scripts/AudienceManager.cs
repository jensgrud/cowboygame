﻿using UnityEngine;
using System.Collections.Generic;

public class AudienceManager : MonoBehaviour {

	public List<GameObject> crowdPrefabs;
	public float crowdHeight;
	public float crowdDistance;
	public float crowdScale;
	public float crowdMax = 20;
	public float spawnValue;
	private float spawnCounter = 0;
	// Use this for initialization

	
	public static AudienceManager Instance {
		get;
		private set;
	}
	
	void Awake(){
		Instance = this; 
	}


	public void SpawnCrowd(){

		Vector3 position;
		for (int i= 0; i< spawnValue; i++) {
		
			float ypos = Random.Range (-1f, 1f);
		
			position.y = Camera.main.transform.position.y - crowdHeight + ypos;
			position.z = crowdDistance + Random.Range (-1f, 1f);
            position.x = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(Screen.width / 3.2f, Screen.width - Screen.width / 3.2f), Screen.height / 2, position.z)).x;
			Quaternion rotation = Quaternion.LookRotation (-Vector3.forward);
			if(spawnCounter < crowdMax){
				GameObject crowd = Instantiate (getCrowdPrefab(), position, rotation) as GameObject;

                float newCrowdScale = crowdScale * (1f - ypos / 1.2f);
				crowd.transform.localScale = new Vector3 (newCrowdScale, newCrowdScale, newCrowdScale);
				spawnCounter++;
			}
		}
	}
	
	private GameObject getCrowdPrefab(){
		return crowdPrefabs[Random.Range(0,crowdPrefabs.Count)];
	}

}
