﻿using UnityEngine;
using System.Collections.Generic;

public class ProgressionManager : MonoBehaviour {

	[System.Serializable]
	public struct PointsAndAmount {
		public int Points;
		public int AllowedJugglables;
	}

	public List<PointsAndAmount> ProgressionList;


	public bool allowsAnotherJugglable {
	
		get {
			
			int jugglablesInTheAir = GameManager.Instance.ObjectsInJuggle().Count;
			int score = ScoreManager.Instance.currentScore;
			
			int allowedJuggles = 1;
			foreach(PointsAndAmount pa in ProgressionList){
				
				if (pa.Points > score)
					break;
				
				allowedJuggles = pa.AllowedJugglables;
			}
			
			return allowedJuggles > jugglablesInTheAir;
		}
	}
}
