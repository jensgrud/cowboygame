﻿using UnityEngine;
using System.Collections.Generic;

public class GameAnalyticsEventsCollector : MonoBehaviour {

	public static GameAnalyticsEventsCollector Instance {
		get; 
		private set;
	}
	
	private Dictionary<int, float> timesUserJugglesAmountOfThings = new Dictionary<int, float>();
	
	void Awake(){
		Instance = this;
		
		//timesUserJugglesAmountOfThings[0] = 0f;
	}
	/*
	void Update(){
		if (GameManager.Instance.IsGameRunning())
		timesUserJugglesAmountOfThings[GameManager.Instance.ObjectsInTheAir.Count] += Time.deltaTime;
	}
	
	private float startTime;
	public void StartNewGame(){
		startTime = 0f;
		timesUserJugglesAmountOfThings.Clear();
		timesUserJugglesAmountOfThings[0] = 0f;
	}
	*/
	
	public void LogGameTime(float GameTime){
		GA.API.Design.NewEvent("Game:Time",GameTime);
	}
	
	public void LogGameScore(int gameScore){
		GA.API.Design.NewEvent("Game:Score",gameScore);
	}
}
