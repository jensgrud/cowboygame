﻿using UnityEngine;
using System.Collections.Generic;

public class HighscoreManager : MonoBehaviour {

	public struct HighScore
	{
		public int score;
		public float time;
		public HighScore(int s, float t){
			score = s;
			time = t;
		}
	}

	
	public int amountOfHighScoresToRemember = 3;
	
	private HighScore lastScore;
	private int numberOfHoles;
	public bool IsCurrentScoreHighScore = false;

	public static HighscoreManager Instance {
		get; 
		private set;
	}
	void Awake(){
		Instance = this;
		lastScore = new HighScore (0, 0);
		numberOfHoles = 0;
		DontDestroyOnLoad(transform.gameObject);
	}



	//Check if the score is a highscore, if it is it's saved
	public bool SaveHighscore(int currentScore, float currentTime, float hits){

		numberOfHoles = (int) hits;

		SaveLastScore (currentScore, Timer.Instance.currentValue);

		HighScore newScore = new HighScore(currentScore, currentTime);

		List<HighScore> highScores = GetHighScoresList();
		
		int positionInHighScores = 0;
		foreach (HighScore highScore in highScores){
		
			if (currentScore > highScore.score){
				highScores.Insert(positionInHighScores,newScore);
				break;
			}
			
			positionInHighScores++;
		}
		
		if (positionInHighScores < highScores.Count)
			saveHighScoresList(highScores);
	
		IsCurrentScoreHighScore = positionInHighScores == 0;
	
		return IsCurrentScoreHighScore;
	}

	//save the value of the last score
	public void SaveLastScore(int score, float time){
		lastScore.score = score;
		lastScore.time = time;

	}

	public float GetLastScore(){

		return lastScore.score;

	}

	public float GetLastTime(){

		return lastScore.time;

	}

	public int GetHits(){
		return numberOfHoles;
	}

	//return a list with all the highscores
	public List<HighScore> GetHighScoresList(){
	
		List<HighScore> ret = new List<HighScore>();
		HighScore tempScore;
	
		for (int i=0; i<amountOfHighScoresToRemember; ++i){
		
			if (!PlayerPrefs.HasKey(i+"th_score")){
				PlayerPrefs.SetInt(i+"th_score",0);
				PlayerPrefs.SetFloat(i+"th_time", 0);
			}
			tempScore.score = PlayerPrefs.GetInt(i+"th_score");
			tempScore.time = PlayerPrefs.GetFloat(i+"th_time");
			ret.Add(tempScore);	
		}
				
		return ret;
	}

	public List<int> GetScoresList(){

		List<HighScore> ret = new List<HighScore>();
		List<int> scoreList = new List<int>();

		for (int i=0; i<amountOfHighScoresToRemember; ++i){
			
			if (!PlayerPrefs.HasKey(i+"th_score")){
				PlayerPrefs.SetInt(i+"th_score",0);
				PlayerPrefs.SetFloat(i+"th_time", 0);
			}
			scoreList.Add(PlayerPrefs.GetInt(i+"th_score"));	
		}
		
		return scoreList;
		
	}

	public List<float> GetTimesList(){
		
		List<HighScore> ret = new List<HighScore>();
		List<float> timeList = new List<float>();;
		
		for (int i=0; i<amountOfHighScoresToRemember; ++i){
			
			if (!PlayerPrefs.HasKey(i+"th_score")){
				PlayerPrefs.SetInt(i+"th_score",0);
				PlayerPrefs.SetFloat(i+"th_time", 0);
			}

			timeList.Add(PlayerPrefs.GetFloat(i+"th_time"));	
		}
		
		return timeList;
		
	}
	
	private void saveHighScoresList(List<HighScore> highScores){
		
		for (int i=0; i<amountOfHighScoresToRemember; ++i){
			PlayerPrefs.SetInt(i+"th_score",highScores[i].score);
			PlayerPrefs.SetFloat(i+"th_time",highScores[i].time);
		}
				
	}
}
