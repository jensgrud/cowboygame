﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlendInCrowd : MonoBehaviour {

	public float blendTime = 1f;

	// Use this for initialization
	void Start () {
		StartCoroutine("BlendIn");
	}
	
	private IEnumerator BlendIn(){
		
		List<MeshRenderer> renderers = new List<MeshRenderer>();
		renderers.AddRange(GetComponentsInChildren<MeshRenderer>());
		
		foreach(MeshRenderer r in renderers){
			Color c = r.material.color;
			c.a = 0f;
			r.material.color = c;
		}
		
		float timer = 0f;
		yield return 0f;
		
		while (timer < blendTime){
		
			timer += Time.deltaTime;
			
			foreach(MeshRenderer r in renderers){
				Color c = r.material.color;
				c.a = (timer / blendTime);
				r.material.color = c;
			}
			
			yield return 0f;
		}
		
		foreach(MeshRenderer r in renderers){
			Color c = r.material.color;
			c.a = 1f;
			r.material.color = c;
		}
	}
}
