﻿using UnityEngine;
using System.Collections;

public class AnimateCowboy : MonoBehaviour {

		Animator animator;
		private float currentAnim = 0;
		private bool animate = false;
		
		public static AnimateCowboy Instance {
			get;
			private set;
		}
		
		void Awake(){
			Instance = this;		
		}
		
		void Start () {
			animator = GetComponentInChildren<Animator> ();
			float currentAnim = animator.GetFloat ("animChange");
			
		}
		void Update () {
			if (animate) {
				currentAnim +=1;
				
				animate = false;
				
				if (currentAnim == 17)
				{
					currentAnim = 1;
				}
				
				animator.SetFloat("animChange", currentAnim);
			}
			
		}
		
		public void Animation () {
			animate = true;
			
		}
	}
