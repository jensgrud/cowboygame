﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

	public float currentValue;

	public static Timer Instance {
		get;
		private set;
	}

	void Awake(){
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		currentValue = 0f;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (GameManager.Instance.IsGameRunning()){
			currentValue += Time.deltaTime;
		}
	}
	
	public void Restart(){
		currentValue = 0f;
	}
}
