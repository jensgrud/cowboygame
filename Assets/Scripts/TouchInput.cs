﻿// This class will take care of
// * Touch on the GUI
// * Touch on the object in the air
// * Touch everywhere else
// *
//

using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour {
	private bool GUIHit = false;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		CheckTouch ();
    }

	void CheckTouch()
	{	
		if (Input.GetMouseButtonDown (0)) {

			//Check if we hit a gui element
			if(GUIUtility.hotControl != 0){
				//we hit a gui element and dont shoot
			}else{
				if(GameManager.Instance.IsGameRunning()){
					ShootingManager.Instance.ShootBullet(Input.mousePosition);
				} 
			}
		}
	}
}