﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class HelpMenu: MonoBehaviour {

	public Texture backTexture;
	public Texture backOnTexture;
	public Texture helpTexture;
	public Font gameFont;
	private float standardRatio;
	private float screenRatio;
	private float conversionRate;
	private float standardWidth;
	private float standardHeight;
	private float widthRate;
	private float heightRate;
	private bool goBack = false;
	private bool isInstruction = false;
	private GUIStyle gameStyle;
	// Use this for initialization
	void Start () {
		gameStyle = new GUIStyle();
		gameStyle.font = gameFont;
		gameStyle.fontSize = 150;
		gameStyle.normal.textColor = Color.white;
		gameStyle.alignment = TextAnchor.UpperCenter;
		standardRatio =(float) 16 / 9; //the gui was designed for 16:9 (1618x910 pixels)
		standardWidth = 1618f;
		standardHeight = 910f; 
		screenRatio = (float) Screen.width / Screen.height;
		conversionRate = screenRatio / standardRatio;
		widthRate = Screen.width / standardWidth;
		heightRate = Screen.height / standardHeight;
		
	}
	void Update(){
		//		screenRatio = (float) Screen.width / Screen.height;
		//		conversionRate = screenRatio / standardRatio;
		//		widthRate = Screen.width / standardWidth;
		//		heightRate = Screen.height / standardHeight;
		
	}
	void OnGUI()
	{
		

		this.jugglableGUI ();
		this.bonusGUI ();
		this.comboGUI ();
		this.instructionGUI ();
		if (!goBack)
						this.backGUI ();
				else
						this.goBackGUI ();
		this.SignGUI ();
	}
	
	void instructionGUI(){
		
		float xScale = 100;
		float yScale = 100;
		float xPosition = Screen.width * 0.23f;
		float yPosition = Screen.height * 0.5f ;
		//	screenRatio = (float) Screen.width / Screen.height;
		gameStyle.fontSize = (int) (70*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		
		GUI.Label (new Rect (xPosition,yPosition, 0, 0), LanguageManager.Instance.GetTextValue("help.can"), gameStyle);
	}
	
	void jugglableGUI(){
		float xScale = 100;
		float yScale = 100;
		float xPosition = Screen.width * 0.29f;
		float yPosition = Screen.height * 0.62f ;
		//	screenRatio = (float) Screen.width / Screen.height;
		gameStyle.fontSize = (int) (70*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		GUI.Label (new Rect (xPosition, yPosition, 0, 0), LanguageManager.Instance.GetTextValue("help.juggle"), gameStyle);

	}
	
	void bonusGUI(){
		float xScale = 100;
		float yScale = 100;
		float xPosition = Screen.width * 0.3f;
		float yPosition = Screen.height * 0.75f ;
		//	screenRatio = (float) Screen.width / Screen.height;
		gameStyle.fontSize = (int) (70*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		GUI.Label (new Rect (xPosition, yPosition, 0, 0), LanguageManager.Instance.GetTextValue("help.bonus"), gameStyle);

		
	}
	
	void comboGUI(){
		float xScale = 100;
		float yScale = 100;
		float xPosition = Screen.width * 0.35f;
		float yPosition = Screen.height * 0.87f ;
		//	screenRatio = (float) Screen.width / Screen.height;
		gameStyle.fontSize = (int) (70*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		GUI.Label (new Rect (xPosition, yPosition, 0, 0), LanguageManager.Instance.GetTextValue("help.combo"), gameStyle);
		
	}
	
	
	void SignGUI(){
		
		gameStyle.fontSize = (int) (120*conversionRate*widthRate);
		float xPosition = Screen.width * 0.5f;
		float yPosition = Screen.height * 0.18f;

		float xScale = 100*conversionRate*widthRate;
		float yScale = 100*conversionRate*heightRate;
		//yPosition = signTexture.height / 6*conversionRate*heightRate;
		gameStyle.normal.textColor = Color.white;
		GUI.Label (new Rect (xPosition-xScale/2, yPosition, xScale, yScale), LanguageManager.Instance.GetTextValue("help.help"), gameStyle);
		
	}
	

	void backGUI(){
		GUIStyle temp = new GUIStyle();
		float xScale = backTexture.width * 0.3f*conversionRate*widthRate;
		float yScale = backTexture.height * 0.3f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.0f;
		float yPosition = Screen.height- yScale;
		//GUI.DrawTexture (new Rect(Screen.width / 2 - replayOff.width/8, Screen.height*0.8f - replayOff.height/4, replayOff.width/4, replayOff.height/4), replayOff);
		if (GUI.Button (new Rect (xPosition, yPosition, xScale, yScale), backTexture, temp))
						goBack = true;
	}
	
	void goBackGUI(){
		float xScale = backOnTexture.width * 0.3f*conversionRate*widthRate;
		float yScale = backOnTexture.height * 0.3f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.0f;
		float yPosition = Screen.height- yScale;
		//GUI.DrawTexture (new Rect(Screen.width / 2 - replayOff.width/8, Screen.height*0.8f - replayOff.height/4, replayOff.width/4, replayOff.height/4), replayOff);
		GUI.DrawTexture (new Rect (xPosition, yPosition, xScale, yScale), backOnTexture);
		StartCoroutine ("GoBack");
	}

	IEnumerator GoBack(){
		
		yield return new WaitForSeconds (0.5f);
		Application.LoadLevel ("StartingMenu");
	}
}