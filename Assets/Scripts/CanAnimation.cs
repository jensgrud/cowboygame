﻿using UnityEngine;
using System.Collections;

public class CanAnimation : MonoBehaviour {

	public static CanAnimation Instance {
		get;
		private set;
	}
	public float animationTime = 0.5f;
	Animator anim;
	//Animation animation;

	void Awake(){
	Instance = this;
	anim = GetComponent<Animator> ();
	//animation = GetComponent<Animation> ();
		
	}


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
	}

	public void CanAnimate(){


		//float xpos = anim.bodyPosition.x -80;
		//float ypos = anim.bodyPosition.y -150;
		//anim.bodyPosition.x = xpos;
		//anim.bodyPosition.y = ypos;
		//OpenDoor ();
		StartCoroutine ("StartAnimate");
	}

	public IEnumerator StartAnimate(){//float canXpos, float canYpos){
		//transform.position = Vector3 (canXpos, canYpos, 0);
		anim.SetBool("canHit", true);
		
//		Vector3 tempV = transform.parent.position;
//		tempV.y -= 1.1f;
//		transform.parent.position = tempV;

		//Debug.Log ("animate");
		
		yield return new WaitForSeconds (animationTime);
//		tempV.y += 1.1f;
//		transform.parent.position = tempV;
		anim.SetBool("canHit", false);

		//transform.parent.position.y = transform.parent.position.y + 6.28;
	}
}
