﻿using UnityEngine;
using System.Collections;

public class PlaygroundManager : MonoBehaviour {

	public static PlaygroundManager Instance {
		get;
		private set;
	}
	public float xOffsetLeftwallBonus = 1f;
	public float yOffsetLeftwallBonus = 1.5f;
	public GameObject ground;
	public GameObject leftWall;
	public GameObject rightWall;
	public float sidewardsWallPush = 20.0f;
	public float upwardsWallPush = 5.0f;
	
	public GameObject JugglableSpawner;
	public GameObject BonusSpawner;

	void Awake(){
		Instance = this;
		Vector3 leftEdgeBottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0,0, -transform.position.z));
		Vector3 rightEdgeBottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(1,0, -transform.position.z));
		
		leftWall.transform.position = new Vector3(leftEdgeBottomCorner.x , leftEdgeBottomCorner.y, 0);
		rightWall.transform.position = new Vector3(rightEdgeBottomCorner.x, rightEdgeBottomCorner.y, 0);
		
		BonusSpawner.transform.position = new Vector3(leftEdgeBottomCorner.x + xOffsetLeftwallBonus , yOffsetLeftwallBonus + leftEdgeBottomCorner.y /	 2, 0);
		JugglableSpawner.transform.position = new Vector3(rightEdgeBottomCorner.x - 2, rightEdgeBottomCorner.y / 2, 0);
	}
}
