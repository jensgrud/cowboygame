﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;
public class EndingMenuManager : MonoBehaviour {

	public Texture signTexture;
	public Texture menuTexture;
	public Texture replayOn;
	public Texture replayOff;
	public Texture canTexture;
	public Font gameFont;
	private bool replay = false;
	private GUIStyle gameStyle;
	private bool isOpen = true;
	private float standardRatio;
	private float screenRatio;
	private float conversionRate;
	private float standardWidth;
	private float standardHeight;
	private float widthRate;
	private float heightRate;
	// Use this for initialization
	void Start () {
		gameStyle = new GUIStyle();
		gameStyle.font = gameFont;
		gameStyle.fontSize = 150;
		gameStyle.normal.textColor = Color.white;
		gameStyle.alignment = TextAnchor.UpperCenter;
		StartCoroutine ("CloseDoor");
		standardRatio =(float) 16 / 9; //the gui was designed for 16:9 (1618x910 pixels)
		standardWidth = 1618f;
		standardHeight = 910f; 
		screenRatio = (float) Screen.width / Screen.height;
		conversionRate = screenRatio / standardRatio;
		widthRate = Screen.width / standardWidth;
		heightRate = Screen.height / standardHeight;
	}
	void Update(){
		standardRatio =(float) 16 / 9; //the gui was designed for 16:9 (1618x910 pixels)
		standardWidth = 1618f;
		standardHeight = 910f; 
		screenRatio = (float) Screen.width / Screen.height;
		conversionRate = screenRatio / standardRatio;
		widthRate = Screen.width / standardWidth;
		heightRate = Screen.height / standardHeight;
		
	}
	IEnumerator CloseDoor(){

		GameObject door = GameObject.FindGameObjectWithTag ("Door");
		Animator doorAnimator = door.GetComponent<Animator> ();
		doorAnimator.SetTrigger ("isClose");
		door.audio.Play ();
		Debug.Log (doorAnimator.animation.clip.length);
		yield return new WaitForSeconds (doorAnimator.animation.clip.length);
		isOpen = false;
	}

	void OnGUI()
	{
		
		//	this.backgroundGUI ();
		if (!isOpen) {
			SignGUI ();
			ScoreGUI ();
			TimeGUI ();
			BestScoresGUI ();

			if (!replay)
					ReplayOffGUI ();
			else
					ReplayOnGUI ();
			CanGUI ();
		}
	}
	void SignGUI(){

		gameStyle.fontSize = (int) (120*conversionRate*widthRate);
		float xScale = signTexture.width*0.6f*conversionRate*widthRate;
		float yScale = signTexture.height*0.4f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.5f;
		float yPosition = -Screen.height * 0.05f;

		GUI.DrawTexture (new Rect(xPosition-xScale/2, yPosition, xScale, yScale), signTexture);

		xScale = 100*conversionRate*widthRate;
		yScale = 100*conversionRate*heightRate;
		yPosition = signTexture.height / 6*conversionRate*heightRate;
		gameStyle.normal.textColor = Color.white;
		GUI.Label (new Rect (xPosition-xScale/2, yPosition, xScale, yScale), LanguageManager.Instance.GetTextValue("end.highscores"), gameStyle);

	}

	void ScoreGUI(){
		gameStyle.fontSize = (int) (80*conversionRate*widthRate);
		float xScale = 200*conversionRate*widthRate;
		float yScale = 50*conversionRate*heightRate;
		float xPosition = Screen.width * 0.25f;
		float yPosition = Screen.height * 0.6f;
		GUI.Label (new Rect (xPosition, yPosition - yScale/2, xScale, yScale),  LanguageManager.Instance.GetTextValue("end.score") +" "+ HighscoreManager.Instance.GetLastScore(), gameStyle);

	}

	void TimeGUI(){
		gameStyle.fontSize = (int) (80*conversionRate*widthRate);
		GUIStyle temp = new GUIStyle();
		float xScale = 200*conversionRate*widthRate;
		float yScale = 50*conversionRate*heightRate;
		float xPosition = Screen.width * 0.25f;
		float yPosition = Screen.height * 0.75f;
		GUI.Label (new Rect (xPosition, yPosition - yScale/2, xScale, yScale), LanguageManager.Instance.GetTextValue("end.time") + HighscoreManager.Instance.GetLastTime().ToString("F2")+" s", gameStyle);
		
	}

	void ReplayOffGUI(){
		GUIStyle temp = new GUIStyle();
		float xScale = replayOff.width * 0.4f*conversionRate*widthRate;
		float yScale = replayOff.height * 0.4f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.5f;
		float yPosition = Screen.height * 0.88f;
		//GUI.DrawTexture (new Rect(Screen.width / 2 - replayOff.width/8, Screen.height*0.8f - replayOff.height/4, replayOff.width/4, replayOff.height/4), replayOff);
		if (GUI.Button (new Rect (xPosition - xScale/2, yPosition - yScale/2, xScale, yScale), replayOff, temp))
						replay = true;
	}

	void ReplayOnGUI(){
		float xScale = replayOn.width * 0.4f*conversionRate*widthRate;
		float yScale = replayOn.height * 0.4f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.5f;
		float yPosition = Screen.height * 0.88f;
		GUI.DrawTexture (new Rect(xPosition - xScale/2, yPosition - yScale/2, xScale, yScale), replayOn);
		StartCoroutine ("RestartGame");
	}

	IEnumerator RestartGame(){

		yield return new WaitForSeconds (0.5f);
        GameManager.Instance.Restart();
	}

	void CanGUI(){
		float xScale = canTexture.width * 1.5f*conversionRate*widthRate;
		float yScale = canTexture.height * 1.5f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.1f;
		float yPosition = Screen.height * 0.7f;
		GUIUtility.RotateAroundPivot (-20.0f, new Vector2(xPosition, yPosition));
		GUI.DrawTexture (new Rect(xPosition - xScale/2, yPosition-yScale/2, xScale, yScale), canTexture);

		GUI.Label (new Rect (xPosition - xScale / 2, yPosition, xScale, yScale), HighscoreManager.Instance.GetHits ().ToString(), gameStyle);


	}
	void BestScoresGUI(){
		gameStyle.fontSize = (int) (80*conversionRate*widthRate);
		List<int> scores = HighscoreManager.Instance.GetScoresList (); 
		List<float> times = HighscoreManager.Instance.GetTimesList ();
		float xScale = 550*conversionRate*widthRate;
		float yScale = 50*conversionRate*heightRate;
		float xPosition = Screen.width * 0.6f;
		float yPosition = Screen.height;
		GUI.Label (new Rect (xPosition, yPosition *0.5f, xScale, yScale), "1. "+scores[0]+"..."+times[0].ToString("F2")+" s", gameStyle);
		GUI.Label (new Rect (xPosition, yPosition *0.65f, xScale, yScale), "2. "+scores[1]+"..."+times[1].ToString("F2")+" s", gameStyle);
		GUI.Label (new Rect (xPosition, yPosition *0.8f, xScale, yScale), "3. "+scores[2]+"..."+times[2].ToString("F2")+" s", gameStyle);
	}

}
