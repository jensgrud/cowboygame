﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision c){
		if (c.gameObject.GetComponent<Shootable>() != null) {
			float localBounceMagnitude = PlaygroundManager.Instance.sidewardsWallPush;
			if(c.gameObject.rigidbody.velocity.x < 0){
				localBounceMagnitude = -localBounceMagnitude;
			}
			c.gameObject.rigidbody.velocity = new Vector3(-c.gameObject.rigidbody.velocity.x + localBounceMagnitude , PlaygroundManager.Instance.upwardsWallPush, 0);
		}
	}
}
