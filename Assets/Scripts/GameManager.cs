﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager Instance {
		get; 
		private set;
	}
	private bool running = true;
	private bool slowedDown = false;

	public bool automaticRestart = true;

	[HideInInspector]
	public List<Shootable> ObjectsInTheAir;
	
	public List<Jugglable> ObjectsInJuggle(){
	
		List<Jugglable> ret = new List<Jugglable>();
		foreach(Shootable obj in ObjectsInTheAir){
			if (obj is Jugglable && ((Jugglable)obj).hitAmount > 0 && !obj.isDead)
				ret.Add(((Jugglable)obj));
		}	
	
		return ret;
	}

	void Awake(){
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		//Timer.onEndTime += EndGame;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public IEnumerator LaggingRestart(){
		yield return new WaitForSeconds (2);
		Restart ();
	}

	private IEnumerator TimeSlowDownCount(float effectTime){
		slowedDown = true;
		Time.fixedDeltaTime = Time.timeScale = 0.5f;
		Time.fixedDeltaTime = 0.02f * Time.timeScale;
		AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		for (int i=0; i < sources.Length; i++) {
			sources[i].pitch = Time.timeScale;
		}
		yield return new WaitForSeconds (effectTime);
		Time.fixedDeltaTime = Time.timeScale = 1.0f;
		Time.fixedDeltaTime = 0.02f * Time.timeScale;
		sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		for (int i=0; i < sources.Length; i++) {
			sources[i].pitch = Time.timeScale;
		}
		slowedDown = false;
	}


	public void EndGame(){
		this.running = false;
		GUIManager.Instance.endGame = true;
		
		ScoreManager.Instance.SaveHighscore();
		/*GameAnalyticsEventsCollector.Instance.LogGameTime(Timer.Instance.currentValue);
		GameAnalyticsEventsCollector.Instance.LogGameScore(ScoreManager.Instance.currentScore);*/
		
//		if (automaticRestart) {
//			StartCoroutine(LaggingRestart());
//		}
	}

	public void Restart(){
		Application.LoadLevel ("MainScene");
		GUIManager.Instance.endGame = false;
		this.running = true;
		Timer.Instance.Restart();
		Time.fixedDeltaTime = Time.timeScale = 1.0f;
		Time.fixedDeltaTime = 0.02f * Time.timeScale;
		AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		for (int i=0; i < sources.Length; i++) {
			sources[i].pitch = Time.timeScale;
		}
	}

	public void StartGame(){
		Application.LoadLevel ("MainScene");
		this.running = true;
		Timer.Instance.Restart ();
		Time.fixedDeltaTime = Time.timeScale = 1.0f;
		Time.fixedDeltaTime = 0.02f * Time.timeScale;
		AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		for (int i=0; i < sources.Length; i++) {
			sources[i].pitch = Time.timeScale;
		}
	}

	public bool IsGameRunning(){
		return this.running;
	}

	public void SlowDownTime(float effectTime){
		if(!slowedDown){
			StartCoroutine(TimeSlowDownCount(effectTime));
		}
	}

    public bool IsSlowedDown()
    {
        return slowedDown;
    }
}
