﻿using UnityEngine;
using System.Collections.Generic;

public class ItemSpawner : MonoBehaviour {

	[System.Serializable]
	public struct ItemSpawnInfo{
		public GameObject prefab;
		public float spawnFrequency;
		public float spawnRandomnessPercent;
		public int minObjectsInTheAir;
		public int minPoints;		
		public Vector3 InitialVelocity;
		public float velocityRandomnessPercent;
	}

	public int spawnSecondCan;
	public int spawnThirdCan;
	public int spawnFourthCan;
	private int cansInAir = 1;
	private bool spawnCan = false;

	public List<ItemSpawnInfo> ItemsToSpawn;
	
	private List<float> timeoutsToNextSpawn = new List<float>();
	
	void Start(){
	
		//start all the item spawn timers
		foreach(ItemSpawnInfo spawnItem in ItemsToSpawn){
			timeoutsToNextSpawn.Add(
				randomizeNextFloat(spawnItem.spawnFrequency,spawnItem.spawnRandomnessPercent)
			);
		}
	}
		
	void Update(){
		//This is bad but the list was bugged
		if (cansInAir == 1 && ScoreManager.Instance.currentScore > spawnSecondCan){
			spawnCan = true;
			Debug.Log(spawnSecondCan);
			Debug.Log(cansInAir);
		}else if (cansInAir == 2 && ScoreManager.Instance.currentScore > spawnThirdCan){
			spawnCan = true;
			Debug.Log("3");
		}else if (cansInAir == 3 && ScoreManager.Instance.currentScore > spawnFourthCan){
			spawnCan = true;
			Debug.Log("4");
		}

		for (int i = 0; i<ItemsToSpawn.Count;++i){
			timeoutsToNextSpawn[i] -= Time.deltaTime;
			
			if (timeoutsToNextSpawn[i] <= 0f){
			
				//recharge
				timeoutsToNextSpawn[i] = randomizeNextFloat(
					ItemsToSpawn[i].spawnFrequency, ItemsToSpawn[i].spawnRandomnessPercent
				);
				
				//if there's enough items in the air to spawn it and it is not a can
				if (
					ItemsToSpawn[i].minObjectsInTheAir <= GameManager.Instance.ObjectsInTheAir.Count
					&&
					ItemsToSpawn[i].minPoints <= ScoreManager.Instance.currentScore
					&& 
					GameManager.Instance.IsGameRunning() 
					&& 
					ItemsToSpawn[i].prefab.name != "Can"
					&& 
					(
						(GetComponent<ProgressionManager>() == null) 
						||
						(GetComponent<ProgressionManager>().allowsAnotherJugglable) 
					)
					){
					
					//spawn!
					SpawnObject(ItemsToSpawn[i]);
				//else if it is a can and we want to spawn it
				}else if (spawnCan && ItemsToSpawn[i].prefab.name == "Can"){
					SpawnObject(ItemsToSpawn[i]);
					spawnCan = false;
					cansInAir++;
				}
			}
		}
	}
	
	public void SpawnObject(ItemSpawnInfo itemSpawnInfo){
		GameObject item = Instantiate(itemSpawnInfo.prefab) as GameObject;
		item.transform.parent = this.transform.parent;
		item.transform.position = this.transform.position;
		item.rigidbody.velocity = itemSpawnInfo.InitialVelocity * 
									randomizeNextFloat(1f, itemSpawnInfo.velocityRandomnessPercent);
	}
	
	//generates a float which randomnessPercent around average
	private float randomizeNextFloat(float average, float randomnessPercent){
		
		float minFloat = average * (1-(randomnessPercent*0.01f));
		float maxFloat = average * (1+(randomnessPercent*0.01f));
		
		return Random.Range(minFloat,maxFloat);
		
	}
}
