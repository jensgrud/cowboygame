﻿// * Responsibilities:
// * Singelton
// * Bullet counts
// * Reload of bullets
// * Shoot when there is bullets
// * Do not sheet when there is no bullets
// 

using UnityEngine;
using System.Collections;

public class ShootingManager : MonoBehaviour {

	public int initalAmountOfBullets = 6;
	public int currentAmountOfBullets { get; private set; }
	private bool waitingToShoot = false;
	AudioSource ReloadAudio;
	[HideInInspector]
	public bool reloading = false;

	public float timeBetweenBullets = 0.5f;
	public float reloadTime = 1.5f;
	public bool automaticReload = true;

	public static ShootingManager Instance {
		get;
		private set;
	}

	void Awake(){
		Instance = this;
		ReloadAudio = GetComponent<AudioSource> ();

	}
	
	// Use this for initialization
	void Start () {
		currentAmountOfBullets = initalAmountOfBullets;
	}
	
	// Update is called once per frame
	void Update () {
	}

	private IEnumerator localRemoveLimits(float time){
		float originalTimeBetweenBullets = timeBetweenBullets;
		float originalReloadTime = reloadTime;
		timeBetweenBullets = 0.0f;
		reloadTime = 0.0f;
		yield return new WaitForSeconds (time);
		timeBetweenBullets = originalTimeBetweenBullets;
		reloadTime = originalReloadTime;
	}

	public IEnumerator Reload(){
		if (!reloading) {
			ShootingManager.Instance.reloading = true;
			Debug.Log ("Reloading");
			currentAmountOfBullets = initalAmountOfBullets;
			ShootingManager.Instance.waitingToShoot = true;
            //ReloadAudio.Play();
			yield return new WaitForSeconds (reloadTime);
			ShootingManager.Instance.waitingToShoot = false;
			ShootingManager.Instance.reloading = false;
			
		}
	}

	public void ShootBullet (Vector3 mousePosition) {
		if (!ShootingManager.Instance.waitingToShoot) {
			Ray mouseray = Camera.main.ScreenPointToRay (mousePosition);
			RaycastHit hit;
			Physics.Raycast (mouseray, out hit, 100);
			bool didFire = ShowBulletFireAndDecrement (hit.point);

			if(didFire){
				CowboyAnimate.Instance.Animation();
				if (hit.transform.name == "pianoGuy"){
					PianoGuy.Instance.Shot();
					Debug.Log("Transformhit");
				}
				if (hit.transform != null && hit.transform.tag == "IShootable") {

					Shootable hitCan = hit.transform.GetComponent<Shootable> ();
						if (hitCan != null) {
						
						if(hitCan.firstHit && !hitCan.isDead)
							AudienceManager.Instance.SpawnCrowd();

						hitCan.OnHit (hit.point);


						}
				}

				if(currentAmountOfBullets < 1 && automaticReload){
					
					StartCoroutine (Reload ());
				} else if(currentAmountOfBullets > 0 ){
					StartCoroutine (WaitToShoot ());
				}
			}
		}
	}

	/// <summary>
	/// Show bullet fire and decrement bullets, 
	/// iff bullets left and fire timer not running
	/// </summary>
	/// <param name="point">3D vector for shooting direction/param>
	/// <returns>true if bullet was fired otherwise false </returns>

	public bool ShowBulletFireAndDecrement (Vector3 point) {


		// Check if bullets left
		if (currentAmountOfBullets > 0) {
			GunEffect.Instance.Shoot (point);
			currentAmountOfBullets -= 1;
			return true;
		}

		return false;
	}

	public IEnumerator WaitToShoot(){
		if (timeBetweenBullets > 0.0f) {
			ShootingManager.Instance.waitingToShoot = true;
			yield return new WaitForSeconds (timeBetweenBullets);
			ShootingManager.Instance.waitingToShoot = false;
		}
	}

	public void RemoveLimits(float time){
		if (timeBetweenBullets > 0.0f) {
			StartCoroutine (localRemoveLimits (time));
		}
	}
}
