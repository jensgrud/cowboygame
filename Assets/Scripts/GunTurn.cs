﻿using UnityEngine;
using System.Collections;

public class GunTurn : MonoBehaviour {

	public float speed = 1.0f;

	public static GunTurn Instance {
		get;
		private set;
	}
	void Awake(){
		Instance = this;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//Turning ();
	}

	public void Turning(Vector3 TouchPos)
	{
		Vector3 diff = TouchPos-transform.position;
		diff.z = 0;
		Vector3 newup = Vector3.Cross (Vector3.forward,diff).normalized;
		float sign = 1;
		if (TouchPos.x < transform.position.x)
			sign = -sign;
		Quaternion newRotation = Quaternion.LookRotation(Vector3.forward, newup);
		gameObject.transform.rigidbody.MoveRotation(newRotation);


		

	}
}
