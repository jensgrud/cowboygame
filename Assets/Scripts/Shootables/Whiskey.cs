﻿using UnityEngine;
using System.Collections;

public class Whiskey : Bonus {

	public float effectLength = 3.0f;

	public override void OnHit(Vector3 hitPosition){
		
		if (isDead)
			return;
		
		base.OnHit (hitPosition);
		
		GameManager.Instance.SlowDownTime (effectLength);
		
		GameObject animation = transform.FindChild("WhiskeyExplosion").gameObject;
		animation.SetActive(true);
		animation.transform.parent = GameManager.Instance.transform;
		GameObject.Destroy(animation,1f);
	}
}
