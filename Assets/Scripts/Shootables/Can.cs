﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public delegate void OnCanhitManager(object sender);

public class Can : Jugglable {

	private GameObject canHolder;
	
	public bool thrown = false;
    public event OnCanhitManager OnCanHit;

	void Start(){
		//GameManager.Instance.ObjectsInTheAir.Add(this); is already done in shootable
		//Instance = this;
		GetComponentInChildren<Collider>().enabled = false;
		StartCoroutine ("Throw");
		canHolder = GameObject.Find ("CanHolder");

	}

	void Update(){

		if (!thrown) {
			transform.position = canHolder.transform.position;
			Vector3 handRot = canHolder.transform.rotation.eulerAngles;
			handRot.z += 90;
			transform.rotation = Quaternion.Euler(handRot);
		}
	}

//	void OnCollisionEnter(Collision c)
//	{
//		Debug.Log ("Collided with" + c.transform.name);
//	}

    public override void OnHit(Vector3 hitPosition)
    {
		if (!thrown)
			return;

		if (isDead)
			return;
		
		base.OnHit(hitPosition);
		
        if (OnCanHit != null)
            OnCanHit(this);
    }

	IEnumerator Throw(){
		yield return new WaitForSeconds (1.4f);
		thrown = true;
		//rigidbody.AddForce (new Vector3 (100,100,0));
		rigidbody.velocity = new Vector3 (20f, 20f, 0f);
		yield return new WaitForSeconds (0.2f);
		GetComponentInChildren<Collider>().enabled = true;

	}
}
