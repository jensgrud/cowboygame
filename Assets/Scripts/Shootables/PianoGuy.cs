﻿using UnityEngine;
using System.Collections;

public class PianoGuy : MonoBehaviour {
	public int hitPoints = 1;
	public AudioClip deathSound;
	Animator anim;
	AudioSource themusic;
	public static PianoGuy Instance {
		get;
		private set;
	}
	void Awake(){
		Instance = this;
		anim = GetComponent<Animator> ();
		themusic = GetComponent<AudioSource> ();
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Shot(){
		hitPoints -= 1;
		if (hitPoints < 1) {
				anim.SetTrigger ("isShot");
				audio.Stop();
				audio.volume = 0.5f;
				audio.PlayOneShot (deathSound);
		}
	}
}
