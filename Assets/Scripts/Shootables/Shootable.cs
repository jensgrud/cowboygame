﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public abstract class Shootable : MonoBehaviour{

	public bool firstHit = true;

	public bool isDead = false;

	public List<AudioClip> soundsOnHit;

	void Awake(){
		GameManager.Instance.ObjectsInTheAir.Add(this);
	}

	public virtual void OnHit(Vector3 hitPosition){		
	
		if (soundsOnHit.Count > 0 && !isDead){
		
			int i = Random.Range(0,soundsOnHit.Count);
			audio.PlayOneShot(soundsOnHit[i]);
		}
	}
	
	void OnDestroy(){
		GameManager.Instance.ObjectsInTheAir.Remove(this);
	}

}
