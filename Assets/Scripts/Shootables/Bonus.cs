﻿using UnityEngine;
using System.Collections;

public class Bonus : Shootable {

	public int bonusAmount;
	public string bonusText;

	public override void OnHit(Vector3 hitPosition){

		if (isDead)
			return;
	
		base.OnHit(hitPosition);

		ScoreManager.Instance.OnBonusHit (Camera.main.WorldToScreenPoint(transform.position), bonusAmount, this);

		GameObject.Destroy(this.gameObject,1f);

		isDead = true;
		foreach(Collider c in GetComponentsInChildren<Collider>())
			c.enabled = false;
		foreach(Renderer r in GetComponentsInChildren<Renderer>())
			r.enabled = false;
			
		GameObject.Destroy(this.gameObject,1f);
		
		if (transform.FindChild("appleExplosive_0") != null){
			GameObject animation = transform.FindChild("appleExplosive_0").gameObject;
			animation.SetActive(true);
			animation.transform.parent = GameManager.Instance.transform;
			GameObject.Destroy(animation,0.5f);
		}

	}
}