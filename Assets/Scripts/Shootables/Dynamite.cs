﻿using UnityEngine;
using System.Collections;

public class Dynamite : Bonus {

	public float explosiveForce = 2000.0f;

	//The dynamite will blow all objects in the air away from it
	public override void OnHit(Vector3 hitPosition){
		
		if (isDead)
			return;
			
		base.OnHit(hitPosition);
			
		foreach (Shootable shootable in GameManager.Instance.ObjectsInTheAir) {
			if(shootable != this){
				//Find direction and distance to each object in the air
				Vector3 direction = shootable.transform.position - transform.position;
				float distance = direction.magnitude;
				// normalize direction
				direction = direction / distance;

				//Add force, more the closer the object is
				shootable.rigidbody.AddForce(direction * ((1/distance) * explosiveForce));

			}
		}
		
		GameObject animation = transform.FindChild("DynamiteAnimation").gameObject;
		animation.SetActive(true);
		animation.transform.parent = GameManager.Instance.transform;
		GameObject.Destroy(animation,1f);
		
	}


}
