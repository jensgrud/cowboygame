﻿using UnityEngine;
using System.Collections;

public class Jugglable : Shootable {

	public float forceMagnitude;
	public float horizontallForceScaling;
	public float verticalForceScaling;
	public float hitAmount = 0;
	
	public override void OnHit(Vector3 hitPosition){
	
		if (isDead)
			return;
	
		base.OnHit(hitPosition);
	
		audio.Play ();
		Vector3 shootingDirection = Vector3.Normalize(transform.position - hitPosition);
		shootingDirection.z = 0f;
		shootingDirection.y = Mathf.Abs (shootingDirection.y)+verticalForceScaling;
		shootingDirection.x = shootingDirection.x * horizontallForceScaling;
		shootingDirection = Vector3.Normalize (shootingDirection);
		rigidbody.AddForce (shootingDirection * forceMagnitude, ForceMode.Impulse);
		float torqueSign = Mathf.Sign((transform.position.x - hitPosition.x) * (transform.position.y - hitPosition.y));
		rigidbody.AddTorque (transform.forward * torqueSign, ForceMode.Impulse);
		hitAmount++;
		ScoreManager.Instance.OnJugglableHit(Camera.main.WorldToScreenPoint(transform.position));
		firstHit = false;
	}
}
