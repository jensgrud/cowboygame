﻿using UnityEngine;
using System.Collections;

public class SheriffStar : Bonus {

	public float effectLength = 5.0f;

	public override void OnHit(Vector3 hitPosition){
		if (isDead)
						return;

		ShootingManager.Instance.RemoveLimits (effectLength);

		base.OnHit (hitPosition);
	}
}
