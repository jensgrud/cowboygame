﻿using UnityEngine;
using System.Collections;

public class GunSmoke : MonoBehaviour {
	ParticleSystem SmokeParticles;
	public static GunSmoke Instance {
		get;
		private set;
	}
	void Awake(){
		Instance = this;
		SmokeParticles = GetComponent<ParticleSystem> ();
	}
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		/*if(Input.GetButton(0)) {
			Smoke();
		}*/
	}

	public void Smoke(){
		//print ("smoke");
		SmokeParticles.Stop ();
		SmokeParticles.Clear ();
		SmokeParticles.Play ();
		//GameObject gunSmokePrefab = (GameObject)Instantiate(Resources.Load ("WhiteSmoke"));
		//GameObject crowd = Instantiate (crowdPrefab, position, rotation) as GameObject;
		/*GameObject gunSmoke = Instantiate (gunSmokePrefab, 
                   		transform.InverseTransformPoint(transform.position),
	                   transform.rotation) as GameObject; 
		print ("instance smoke");
		//yield return new WaitForSeconds(3);
		print ("smoke 3 seconds");
		//GameObject.Destroy(gunSmoke);*/
	}
}
