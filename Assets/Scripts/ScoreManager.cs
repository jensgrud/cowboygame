﻿using UnityEngine;
using System;
using System.Collections;

public delegate void ComboStartEventHandler(object sender);
public delegate void ComboEndEventHandler(object sender, ComboEventArgs e);

public class ScoreManager : MonoBehaviour {

    public event ComboStartEventHandler OnComboStart;
    public event ComboEndEventHandler OnComboEnd;

	private float timesHitCan = 0f;

    public int hitValue = 10;
	public int currentScore = 0;
	public int points = 0;

    public int comboExponent = 3;
    public int minJuggleToCombo = 3;
    public int minHitThreshholdToCombo = 10;
    public int comboSoundThreshhold = 1500;
    public AudioClip bonusSoundSmall;
    public AudioClip bonusSoundBig;

    private int hitStreak = 0;
    private int itemsComboed = 0;
    private bool comboStarted = false;

	public static ScoreManager Instance {
		get;
		private set;
	}
	
	void Awake(){
		Instance = this;
	}
	
	public void OnJugglableHit(Vector3 position){
        int inJuggleCount = GameManager.Instance.ObjectsInJuggle ().Count;
        if (inJuggleCount >= minJuggleToCombo)
        {
            hitStreak++;

            if (!comboStarted && hitStreak >= minHitThreshholdToCombo)
            {
                comboStarted = true;

                if (OnComboStart != null)
                    OnComboStart(this);
            }
        }

        //If we dropped an item
        if (itemsComboed > inJuggleCount)
        {
            if (comboStarted)
            {
                int combo = hitStreak * Convert.ToInt32(Math.Pow(itemsComboed, comboExponent));
                currentScore += combo;

                //Audio
                //TODO: This needs review once we get better soundfiles
                AudioSource src = GetComponent<AudioSource>();
                src.clip = (combo < comboSoundThreshhold) ? bonusSoundSmall : bonusSoundBig;
                src.Play();

                if (OnComboEnd != null)
                    OnComboEnd(this, new ComboEventArgs(combo));
            }

            //Reset
            itemsComboed = 1;
            hitStreak = 0;
            comboStarted = false;
        }

        itemsComboed = inJuggleCount;
		points = inJuggleCount * hitValue;
		currentScore += points;
		GUIManager.Instance.ShowPointsOnTap (position);
	}

	public void OnBonusHit(Vector3 position, int bonusPoints, Bonus item){
		points = bonusPoints;
		currentScore += bonusPoints;
		GUIManager.Instance.ShowPointsOnTap (position, item);
	}
	
	public void SaveHighscore(){

		GameObject[] cans = GameObject.FindGameObjectsWithTag ("Can");

		Debug.Log (cans.Length);

		foreach (GameObject can in cans) {
			Debug.Log ("Can");
			timesHitCan += can.transform.parent.GetComponent<Can>().hitAmount;
		}

		bool isItHighScore = 
		GetComponent<HighscoreManager>().SaveHighscore(currentScore, Timer.Instance.currentValue, timesHitCan);	

	//	GetComponent<HighscoreManager> ().SaveLastScore (currentScore, Timer.Instance.currentValue);
	}
}

public class ComboEventArgs : EventArgs
{
    public int comboScore = 0;
    public ComboEventArgs(int comboScore)
    {
        this.comboScore = comboScore;
    }
}
