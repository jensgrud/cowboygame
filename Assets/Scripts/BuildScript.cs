﻿using UnityEditor;

class BuildScript
{
	static string APP_NAME = "HotShotShowoff";
	static string TARGET_DIR = "target";
	
	static void PerformBuild ()
	{
		string[] scenes = { "Assets/Scences/StartScene.unity", "Assets/Scences/MainScene.unity", "Assets/Scences/EndScene.unity"};
		BuildPipeline.BuildPlayer (scenes, TARGET_DIR + "/" + APP_NAME + ".apk", BuildTarget.Android, BuildOptions.None);
	}
}