﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class MenuManager : MonoBehaviour {

	public Texture englishTexture;
	public Texture menuTexture;
	public Texture danishTexture;
	public Texture helpTexture;
 	public Font gameFont;
	private float standardRatio;
	private float screenRatio;
	private float conversionRate;
	private float standardWidth;
	private float standardHeight;
	private float widthRate;
	private float heightRate;
	private bool isOpen = false;
	private bool isInstruction = false;
	private GUIStyle gameStyle;
	// Use this for initialization
	void Start () {
		gameStyle = new GUIStyle();
		gameStyle.font = gameFont;
		gameStyle.fontSize = 150;
		gameStyle.normal.textColor = Color.white;
		gameStyle.alignment = TextAnchor.UpperCenter;
		standardRatio =(float) 16 / 9; //the gui was designed for 16:9 (1618x910 pixels)
		standardWidth = 1618f;
		standardHeight = 910f; 
		screenRatio = (float) Screen.width / Screen.height;
		conversionRate = screenRatio / standardRatio;
		widthRate = Screen.width / standardWidth;
		heightRate = Screen.height / standardHeight;

	}
	void Update(){
//		screenRatio = (float) Screen.width / Screen.height;
//		conversionRate = screenRatio / standardRatio;
//		widthRate = Screen.width / standardWidth;
//		heightRate = Screen.height / standardHeight;

	}
	void OnGUI()
	{

		if (!isOpen) {
						this.titleGUI ();
						this.playGUI ();
						this.englishGUI ();
						this.danishGUI ();
			this.helpGUI();
				}
		if (isInstruction)
						this.instructionGUI ();
	}
	


	void titleGUI(){
		float xScale = 100;
		float yScale = 100;
		float xPosition = Screen.width * 0.5f;
		float yPosition = 0;
	//	screenRatio = (float) Screen.width / Screen.height;
		gameStyle.fontSize = (int) (120*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		GUI.Label (new Rect (xPosition, yPosition, 0, 0), "HOTSHOT", gameStyle);
		yPosition += gameStyle.fontSize*0.8f;
		gameStyle.fontSize = (int) (150*conversionRate*widthRate);
		GUI.Label (new Rect (xPosition, yPosition, 0, 0), "SHOWOFF", gameStyle);
	}

	void danishGUI(){
		gameStyle.fontSize = (int) (70*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		float xScale = menuTexture.width*0.7f*conversionRate*widthRate;
		float yScale = menuTexture.height*0.7f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.7f;
		float yPosition = Screen.height * 0.75f;

		GUI.DrawTexture (new Rect(xPosition-xScale/4, yPosition-yScale/4, xScale, yScale), menuTexture);
		xScale = danishTexture.width * 0.2f*conversionRate*widthRate;
		yScale = danishTexture.height * 0.2f*conversionRate*heightRate;
		GUI.DrawTexture (new Rect(xPosition + 215*conversionRate*widthRate , yPosition + 10*conversionRate*heightRate, xScale, yScale), danishTexture);

		if (GUI.Button (new Rect (xPosition, yPosition, 200*conversionRate*widthRate, 100*conversionRate*heightRate), LanguageManager.Instance.GetTextValue("start.danish"), gameStyle)) {
			LanguageManager.Instance.ChangeLanguage("da");
			LoadSystemLanguageOnStart.savedLanguage = "da";
		}

	}

	void englishGUI(){
		gameStyle.fontSize = (int) (70*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		float xScale = menuTexture.width*0.7f*conversionRate*widthRate;
		float yScale = menuTexture.height*0.7f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.7f;
		float yPosition = Screen.height * 0.55f+gameStyle.fontSize/2;
		GUI.DrawTexture (new Rect(xPosition-xScale/4, yPosition-yScale/4, xScale, yScale), menuTexture);
		xScale = englishTexture.width * 0.18f*conversionRate*widthRate;
		yScale = (englishTexture.height * 0.18f+gameStyle.fontSize/2)*conversionRate*heightRate;
		GUI.DrawTexture (new Rect(xPosition + 225*conversionRate*widthRate , yPosition, xScale, yScale), englishTexture);
		if (GUI.Button (new Rect (xPosition, yPosition, 200*conversionRate*widthRate, 100*conversionRate*heightRate), LanguageManager.Instance.GetTextValue("start.english"), gameStyle)) {
			LanguageManager.Instance.ChangeLanguage("en");
			LoadSystemLanguageOnStart.savedLanguage = "en";
		}
	}


	IEnumerator OpenDoor(){

		isOpen = true;
		GameObject door = GameObject.FindGameObjectWithTag ("Door");
		Animator doorAnimator = door.GetComponent<Animator> ();
		doorAnimator.SetTrigger ("isOpen");
		door.audio.Play ();
		Debug.Log (doorAnimator.animation.clip.length);
		yield return new WaitForSeconds (doorAnimator.animation.clip.length);
		isInstruction = true;
		yield return new WaitForSeconds (2);
		Application.LoadLevel("MainScene");
	}

	void helpGUI(){

		gameStyle.fontSize = (int) (70*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		float xScale = helpTexture.width*1.2f*conversionRate*widthRate;
		float yScale = helpTexture.height*1.2f*conversionRate*heightRate;
		float xPosition = Screen.width-xScale*0.5f;
		float yPosition = Screen.height-yScale*0.78f;
		GUIStyle temp = new GUIStyle();
		GUIUtility.RotateAroundPivot (40.0f, new Vector2(xPosition, yPosition));

		if (GUI.Button (new Rect (xPosition, yPosition, xScale * conversionRate * widthRate, yScale * conversionRate * heightRate), helpTexture, temp)) {
			Application.LoadLevel("Help");
		}
	}

	void instructionGUI(){

		gameStyle.fontSize = (int) (150*conversionRate*widthRate);
		
		GUI.Label (new Rect (Screen.width/2, Screen.height/2, 0, 0), LanguageManager.Instance.GetTextValue("start.instruction"), gameStyle);
	}
	void playGUI(){
		gameStyle.fontSize = (int) (125*conversionRate*widthRate);
		gameStyle.normal.textColor = Color.white;
		float xScale = menuTexture.width*0.8f*conversionRate*widthRate;
		float yScale = menuTexture.height*0.7f*conversionRate*heightRate;
		float xPosition = Screen.width * 0.15f;
		float yPosition = Screen.height * 0.6f;
		GUI.DrawTexture (new Rect(xPosition-xScale/4, yPosition , xScale, yScale), menuTexture);

		if (GUI.Button (new Rect (xPosition, yPosition, 200*conversionRate*widthRate, 100*conversionRate*heightRate), LanguageManager.Instance.GetTextValue("start.play"), gameStyle)) {

			StartCoroutine("OpenDoor");
		}
	}


}
