﻿using UnityEngine;

public class GunEffect : MonoBehaviour
{
	//public float timeBetweenBullets = 0.15f;
	public float maxrange = 100f;
    public AudioClip[] Scruffs;
    public int scruffMin = 8;
    public int scruffMax = 13;
	
	float timer;
	Ray shootRay;
	RaycastHit shootHit;
	ParticleSystem gunParticles;
	LineRenderer gunLine;
	AudioSource gunAudio;
    AudioSource scruffAudio;

	Light gunLight;
	public float effectsDisplayTime = 0.2f;

    private int numShotsFired = 0;
    private int nextScruff;
	
	public static GunEffect Instance {
		get;
		private set;
	}
	
	void Awake ()
	{
		Instance = this;
		gunParticles = GetComponent<ParticleSystem> ();
		gunLine = GetComponent <LineRenderer> ();
		gunLight = GetComponent<Light> ();

        nextScruff = Random.Range(scruffMin, scruffMax);
        foreach (AudioSource src in GetComponents<AudioSource>())
            if (src.clip != null)
                gunAudio = src;
            else
                scruffAudio = src;
	}
	
	
	void Update ()
	{
		timer += Time.deltaTime;
		
		/*if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets)
        {
			Vector3 Derp = new Vector3 (0,0,0);
            Shoot (Derp);
        }*/
		
		if(timer >= ShootingManager.Instance.timeBetweenBullets * effectsDisplayTime)
		{
			DisableEffects ();
		}
		
		
		
	}
	
	public void DisableEffects ()
	{
		gunLine.enabled = false;
		gunLight.enabled = false;
	}
	
	
	public void Shoot (Vector3 TouchPos)
	{
		GunTurn.Instance.Turning(TouchPos);
		//GunSmoke.Instance.Smoke();
		/*Vector3 diff = TouchPos-transform.position;
		
		//diff.z = 0;
		// we do not want to rotate in z axis
		Quaternion newRotation = Quaternion.LookRotation(diff);
		gameObject.transform.rigidbody.MoveRotation(newRotation);*/
		
		timer = 0f;
		
		gunAudio.Play ();
		
		gunLight.enabled = true;
		
		gunParticles.Stop ();
		gunParticles.Play ();

        this.numShotsFired++;

        if (numShotsFired == nextScruff)
        {
            scruffAudio.clip = Scruffs[Random.Range(0, Scruffs.Length)];
            scruffAudio.Play();
            numShotsFired = 0;
            nextScruff = Random.Range(scruffMin, scruffMax);
        }
		/*


        gunLine.enabled = true;
//		Vector3 NewPos = transform.parent.position + transform.forward * 1.6f + transform.up * 0.1f;
//        gunLine.SetPosition (0, NewPos);
//
//		gunLine.SetPosition (1, TouchPos);
		
		
		/*shootRay.origin = transform.position;
		shootRay.direction = transform.forward;

        if(Physics.Raycast (shootRay, out shootHit, maxrange))
        {
            gunLine.SetPosition (1, shootHit.point);
			print (shootHit);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * maxrange);
        }*/
		
		//We make a ray from the guns position and toward its forward(blue) axis and if it hits something then it makes a line 
		//from 1 to shootHit.point which is the target we hit. If we dont hit anything then it just shoots to max range.
		//Audio, linerenderer, light is enabled every time we fire until the timer is up.
	}
	
}
